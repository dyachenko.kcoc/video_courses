import random
import os
import string

from datetime import datetime

from uuid import uuid4

from project import settings


def path_and_rename(instance, filename):
    path = settings.UPLOAD_TO_PATHS[instance.__class__.__name__]
    extension = filename.split('.')[-1]
    random_string = str(uuid4().hex)
    filename = f'{random_string}.{extension}'
    return os.path.join(datetime.strftime(datetime.today(), path), filename)


def random_slug_helper():
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))
