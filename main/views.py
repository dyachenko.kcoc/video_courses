from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import gettext_lazy as _

from main.models import Course, Subject, Lesson


def index(request):
    course = Course.objects.active().first()
    # subjects_list = Subject.objects.active().filter(course=course).order_by('position') # саме той курс course
    subjects_list = []
    if course:
        subjects_list = course.subjects.active().order_by('position')  # related_name = subjects
    # queryset з всіма Subjects, що посилаються на цей курс
    # course.subjects  -  related_name='subjects'  в models.Subject
    # це екземпляр ModelQuerySet сортування,спочатку по position '-' зворотній напрямок
    template_name = 'main/index.html'
    context = {
            'title': _('Головна сторінка курсу'),  # вставити всюди де є текстові фрази
            'subjects_list': subjects_list,  # ModelQuerySet перетворюється всередині шаблону в список
            'course': course,
        }
    return render(request, template_name=template_name, context=context)


def subject_page(request, pk):
    queryset = Subject.objects.active()  # тільки активні предмети
    subject = get_object_or_404(queryset, pk=pk)  # get_object_or_404 витягає з бази об'єкт по pk
    lessons_list = subject.lessons.active().order_by('position')  # список всіх активних уроків, що посилаються на цей subject
    return render(
        request,
        'main/subject_page.html',
        {
            'subject': subject,
            'lessons_list': lessons_list
        }
    )


def lesson_page(request, pk):
    queryset = Lesson.objects.active()  # тільки активні уроки
    lesson = get_object_or_404(queryset, pk=pk)  # get_object_or_404 витягає з бази об'єкт по pk

    lesson_videos = lesson.videos.active().order_by('position')
    return render(
        request,
        'main/lesson_page.html',
        {
            'lesson': lesson,
            'lesson_videos': lesson_videos
        }
    )

# class based views CBV
# get_extra_context
# class LessonDetailView(views.GenericDetailView):
