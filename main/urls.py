from django.urls import path

from main.views import index, subject_page, lesson_page

app_name = 'main'

urlpatterns = [
    path('subject/<int:pk>/', subject_page, name='subject-page'),  # дефіс в імені
    path('lesson/<int:pk>/', lesson_page, name='lesson-page'),  # дефіс в імені
    path('', index, name='home'),
]
