from django.contrib import admin
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from main.models import Course, Subject, Lesson, LessonVideo
# from main.models import User
from modeltranslation.admin import TranslationAdmin

admin.site.site_header = _('Школа Леді')


def admin_method_attributes(**outer_kwargs):
    """ Wrap an admin method with passed arguments as attributes and values.
    DRY way of extremely common admin manipulation such as setting short_description, allow_tags, etc.
    """
    def method_decorator(func):
        for kw, arg in outer_kwargs.items():
            setattr(func, kw, arg)
        return func
    return method_decorator


def switch_active(modeladmin, request, queryset):
    queryset.update(active=Q(active=False))


class ProjectAdmin(TranslationAdmin):
    save_on_top = True
    save_as = True
    actions_on_top = True
    actions = [switch_active, ]
    ordering = ('-created',)  # дефолтне сортування в адмінці
    switch_active.short_description = _('Активувати/Деактивувати вибрані елементи')


@admin.register(Course)
class CourseAdmin(ProjectAdmin):
    pass


@admin.register(Subject)
class SubjectAdmin(ProjectAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Lesson)
class LessonAdmin(ProjectAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(LessonVideo)
class LessonVideoAdmin(ProjectAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_per_page = 50
    list_filter = [
        'created',
        'active',
    ]

# @admin.register(User)
# class UserAdmin(admin.ModelAdmin):
#     pass
