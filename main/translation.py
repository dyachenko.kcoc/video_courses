from modeltranslation.translator import register, TranslationOptions
from .models import Course, Subject, Lesson, LessonVideo


@register(Course)
class CourseTranslationOption(TranslationOptions):
    fields = ['title', 'description']


@register(Subject)
class SubjectTranslationOption(TranslationOptions):
    fields = ['title', 'description']


@register(Lesson)
class LessonTranslationOption(TranslationOptions):
    fields = ['title', 'description']


@register(LessonVideo)
class LessonVideoTranslationOption(TranslationOptions):
    fields = ['title', 'description']
