from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
# from django.contrib.auth.models import AbstractUser

from ckeditor.fields import RichTextField
from versatileimagefield.fields import VersatileImageField, PPOIField

from project.utils import path_and_rename, random_slug_helper


class DeactivatableModelQuerySet(models.QuerySet):
    def active(self):
        return self.filter(active=True)

    def disabled(self):
        return self.filter(active=False)


class AbstractModel(models.Model):
    title = models.CharField(_('Назва'), max_length=255, null=True, blank=True,)
    active = models.BooleanField(_('Активне'), default=True, db_index=True)
    created = models.DateTimeField(_('Створено'), default=timezone.now, db_index=True)
    updated = models.DateTimeField(_('Оновлено'), auto_now=True, db_index=True)
    position = models.PositiveSmallIntegerField(_('Сортування'), null=True, default=0)

    objects = DeactivatableModelQuerySet.as_manager()  #

    class Meta:
        ordering = ['position']
        abstract = True

    def __str__(self):
        return f'{self.title}'


class ContentMixin(models.Model):

    description = RichTextField(_('Description'), null=True, blank=True, )
    image_ppoi = PPOIField()
    slug = models.SlugField(
        _('Унікальний slug сторінки'),
        unique=True,
        max_length=255,
    )
    preview = VersatileImageField(
        ppoi_field='image_ppoi',
        verbose_name=_('Прев`ю'),
        upload_to=path_and_rename,
        help_text='ще треба вирішити розміри',
    )
    published_at = models.DateTimeField(
        _('Published at'),
        null=True,
        blank=True,
        db_index=True,
    )

    def save(self, *args, **kwargs):
        if not self.slug:
            # self.slug = slugify(f'{self.title}-{random_slug_helper()}')
            self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class Course(AbstractModel):
    description = RichTextField(_('Description'), null=True, blank=True, )
    price = models.SmallIntegerField(
        _('Ціна курсу'),
        blank=True,
        db_index=True,
    )

    class Meta:
        verbose_name = _('Курс')
        verbose_name_plural = _('Курси')


class Subject(AbstractModel, ContentMixin):
    course = models.ForeignKey(
        Course,
        verbose_name=_('Курс'),
        related_name='subjects',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _('Предмет')
        verbose_name_plural = _('Предмети')


class Lesson(AbstractModel, ContentMixin):
    subject = models.ForeignKey(
        Subject,
        verbose_name=_('Предмет'),
        related_name='lessons',
        null=True,
        on_delete=models.SET_NULL,
        blank=True,
    )

    class Meta:
        verbose_name = _('Урок')
        verbose_name_plural = _('Уроки')


class LessonVideo(AbstractModel, ContentMixin):
    LOW = '480'
    MIDDLE = '720'
    HIGH = '1080'
    VIDEO_RESOLUTION = (
        (LOW, '480'),
        (MIDDLE, '720'),
        (HIGH, '1080'),
    )
    quality = models.CharField(
        _('Якість відео'),
        max_length=50,
        choices=VIDEO_RESOLUTION,
        db_index=True
    )
    lesson = models.ForeignKey(
        Lesson,
        verbose_name=_('Урок'),
        related_name='videos',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    video_file = models.FileField(upload_to=path_and_rename)

    class Meta:
        verbose_name = _('Відео уроку')
        verbose_name_plural = _('Відео уроків')


# class User(AbstractUser):
    # sex = models.CharField()
    # birthday = models.DateField()
    # phone_number = models.CharField()
    # my_courses = models.ManyToManyField()
    # my_photo = models.ImageField(
    #     upload_to='path_and_rename',
    #     height_field=None,
    #     width_field=None,
    # )
